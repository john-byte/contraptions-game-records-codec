
var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: {
      'cg-parser.es6': './src/index.js',
    },
    output: {
      path: path.resolve(__dirname, 'build-prod'),
      filename: '[name].js',
      libraryTarget: 'umd',
      library: 'CgParser',
      umdNamedDefine: true
    },
    resolve: {
      extensions: ['.js']
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  corejs: 3,
                  debug: true,
                  useBuiltIns: 'usage',
                }]
              ]
            }
          }
        }
      ]
    },
}
  
  