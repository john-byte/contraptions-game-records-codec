import { mimeTable } from './mime/mime';
import { ignoreWhitespacePlus, ignoreWhitespace } from './utils';

function readKey(str, ptr) {
    const token = [];

    while (ptr < str.length && str[ptr] !== ":") {
        token.push(str[ptr]);
        ptr++;
    }

    return [ptr + 1, token.join("")];
}

function readValue(str, ptr) {
    const token = [];

    while (ptr < str.length && str[ptr] !== "\n") {
        token.push(str[ptr]);
        ptr++;
    }

    return [ptr + 1, token.join("")];
}

/**
 * @param {File} file
 * @returns name table 
 */
export async function* parse(file) {
    const rawContent = await file.text();
    const nameTable = {};

    let ptr = 0;
    let key = "";
    let value = "";
    
    let recordName;
    let recordType;
    let recordLength;

    while (ptr < rawContent.length) {
        ptr = ignoreWhitespacePlus(rawContent, ptr);
        [ptr, key] = readKey(rawContent, ptr);

        ptr = ignoreWhitespace(rawContent, ptr);
        [ptr, value] = readValue(rawContent, ptr);

        switch (key) {
            case "name":
                recordName = value;
                break;
            case "type":
                recordType = value;
                break;
            case "length":
                recordLength = value;
                break;
            case "content":
                if (!recordName) {
                    throw `Err(${ptr}): Content type must be specified before to identify it!`;
                }

                if (!recordType) {
                    throw `Err(${ptr}): Content type must be specified before to parse it!`;
                }

                if (!recordLength) {
                    throw `Err(${ptr}): Content length must be specified before to read it!`;
                }

                if (!nameTable[recordName]) {
                    nameTable[recordName] = {
                        $name: recordName,
                        $current: null,
                        $type: 'unknown',
                    };
                }

                const recordLengthNum = Number.parseInt(recordLength);
                const chunk = rawContent.slice(ptr, ptr + recordLengthNum);

                nameTable[recordName].$type = recordType;
                
                if (!mimeTable[recordType]) {
                    nameTable[recordName].$current = chunk;
                    yield nameTable;
                } else {
                    const mimeParser = mimeTable[recordType].parse;
                    const parsedChunk = await mimeParser(chunk, nameTable);
                    
                    nameTable[recordName].$current = parsedChunk;
                    yield nameTable;
                }

                ptr += recordLengthNum;
                break;
        }
    }

    return nameTable;
};

/**
 * @param {{[key: string]: { $current: any, $type: string, $name: string }}} nameTable
 * @returns  
 */
export async function* serialize(nameTable) {
    const res = [];
    let buff;

    for (const name in nameTable) {
        buff = [];

        const record = nameTable[name];
        const recType = record.$type;

        let content;
        if (!mimeTable[recType]) {
            content = record.$current;
        } else {
            const serializer = mimeTable[recType].serialize;
            const chunk = await serializer(record.$current, nameTable);
            content = chunk;
        }

        buff.push("name: ", name, "\n");
        buff.push("type: ", recType, "\n");
        buff.push("length: ", content.length, "\n");
        buff.push("content: \n", content, "\n\n");

        const chunk = buff.join("");
        yield chunk;

        res.push(chunk);
    }

    const str = res.join("");
    return new File([str], "serialized.cgdat");
}