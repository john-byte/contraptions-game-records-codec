/**
 * @param {Function} job 
 */
export const scheduleLowPriorityJob = async (job) => {
    const scheduler = typeof window !== 'undefined' && window.requestIdleCallback ? 
        window.requestIdleCallback : 
        setTimeout;

    await new Promise((resume, interrupt) => {
        scheduler(() => {
            try {
                job();
                resume();
            } catch (e) {
                interrupt(e);
            }
        });
    });
};

const whitespaceRegex = /\s/;
/**
 * @param {string} str 
 * @param {number} ptr 
 * @returns 
 */
export function ignoreWhitespacePlus(str, ptr) {
    while (ptr < str.length && whitespaceRegex.test(str[ptr])) {
        ptr++;
    }

    return ptr;
}

/**
 * @param {string} str 
 * @param {number} ptr 
 * @returns 
 */
export function ignoreWhitespace(str, ptr) {
    while (ptr < str.length && str[ptr] === " ") {
        ptr++;
    }

    return ptr;
}
