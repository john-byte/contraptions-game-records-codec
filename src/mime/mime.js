import { parseJSON, serializeJSON } from './json';
import { parseMeta, serializeMeta } from './meta';

export const mimeTable = {
    "application/json+x-contraptions-scene": {
        parse: parseJSON,
        serialize: serializeJSON,
    },
    "application/x-contraptions-meta": {
        parse: parseMeta,
        serialize: serializeMeta,
    },
}

