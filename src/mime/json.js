import { scheduleLowPriorityJob } from '../utils';

/**
 * @param {string} rawChunk 
 * @param {{[key: string]: { $current: any, $type: string, $name: string }}} nameTable 
 * @returns 
 */
export async function parseJSON(rawChunk, nameTable) {
    let parsedChunk;

    await scheduleLowPriorityJob(() => {
        parsedChunk = JSON.parse(rawChunk, (k, v) => {
            if (typeof v === 'string' && v.length && v[0] === '@') {
                const link = v.slice(1);
    
                if (!nameTable[link]) {
                    nameTable[link] = {
                        $name: link,
                        $current: null,
                        $type: 'unknown',
                    };
                }
    
                return nameTable[link];
            }
    
            return v;
        });
    });

    return parsedChunk;
};

/**
 * 
 * @param {any} content 
 * @param {{[key: string]: { $current: any, $type: string, $name: string }}} nameTable 
 */
export async function serializeJSON(content, nameTable) {
    let res;
    
    await scheduleLowPriorityJob(() => {
        res = JSON.stringify(content, (_, v) => {
            if (typeof v === "object" && v.$name) {
                return `@${v.$name}`;
            }

            return v;
        });
    });

    return res;
}