import { ignoreWhitespace, ignoreWhitespacePlus } from '../utils';

function readKey(str, ptr) {
    const token = [];

    while (ptr < str.length && str[ptr] !== "=") {
        token.push(str[ptr]);
        ptr++;
    }

    return [ptr + 1, token.join("")];
}

function readValue(str, ptr) {
    const token = [];

    while (ptr < str.length && str[ptr] !== "\n") {
        token.push(str[ptr]);
        ptr++;
    }

    return [ptr + 1, token.join("")];
}

/**
 * @param {string} rawContent 
 * @param {{[key: string]: { $current: any }}} nameTable 
 */
export async function parseMeta(rawContent, nameTable) {
    const res = {};

    let key;
    let val;
    let ptr = 0;

    while (ptr < rawContent.length) {
        ptr = ignoreWhitespacePlus(rawContent, ptr);
        [ptr, key] = readKey(rawContent, ptr);
        [ptr, val] = readValue(rawContent, ptr);
        if (!key.length || !val.length) break;

        if (typeof val === "string" && val.length && val[0] === "@") {
            const name = val.slice(1);
            
            if (!nameTable[name]) {
                nameTable[name] = {
                    $name: name,
                    $current: null,
                    $type: 'unknown',
                };
            }

            res[key] = nameTable[name];
        } else {
            res[key] = val;
        }
    }

    return res;
}

/**
 * @param {any} content 
 * @param {{[key: string]: { $current: any, $type: string, $name: string }}} nameTable 
 */
export async function serializeMeta(content) {
    const res = [];
    for (const k in content) {
        const v = content[k];
        if (typeof v === "object" && v.$name) {
            res.push(k, "=", `@${v.$name}`, "\n");
        } else {
            res.push(k, "=", v.toString(), "\n");
        }
    }

    return res.join("");
}